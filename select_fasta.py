#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Gabriel Morales'
"""
import argparse
import re
import adn as module_adn


def create_parser():
    """ Declares new parser and adds parser arguments """

    program_description = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(add_help=True, description=program_description)
    parser.add_argument('-i', '--inputfile', type=str)
    return parser


def main():
    """ read input file, and isolate all the sequences names, sequences 
    lenght and apply the function is_valid() from module adn """

    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args["inputfile"])
    with open(args['inputfile']) as reading_input_file:
        input_file_string = (reading_input_file.read())
        regex_isolate_sequences = r">seq\d[\r\n]+([^\>]+)"
        matches = re.finditer(regex_isolate_sequences, input_file_string, re.MULTILINE | re.DOTALL)
        for match_num, match in enumerate(matches, start=1):
            regex_isolate_name_sequences = r">seq\d+"
            print(re.search(regex_isolate_name_sequences, match.group()).group(0))
            for group_num in range(0, len(match.groups())):
                group_num = group_num + 1
                adn_str = module_adn.purify(match.group(group_num))
                print("Long of the secuence is:", len(adn_str))
                module_adn.is_valid(adn_str)
if __name__ == "__main__":
    main()
