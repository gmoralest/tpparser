#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Gabriel Morales'
"""
import re


def purify(adn_str):
    """ CLEAR ALL THE SPACES AND NEW LINES FROM 
    THE ENTRY STRING  AND RETURN A COMPLETE
    LOWERCASE STRING. """
    
    try:
        adn_str = adn_str.lower()
        adn_str = adn_str.replace("\n", "")
        adn_str = adn_str.replace(" ", "")
    except:
        adn_str = adn_str
    return adn_str


def is_valid(adn_str):
    """ EVALUATES IF ALL THE STRING ITS ONLY COMPOSED 
    BY THE CHARACTERS A,T,C,G, AND, IF NOT THE CASE, RETURN THE 
    INVALID CHARACTER AND HIS POSITION IN THE STRING """
    
    char_list = ["a", "t", "c", "g"]
    if adn_str is None or len(adn_str.replace(" ", "")) == 0:
        print("AN INVALID SEQUENCE")
        return
    matched_list = [characters in char_list for characters in adn_str]
    string_contains_chars = all(matched_list)
    if string_contains_chars:
        print("A valid sequence :", adn_str)
    else:
        print("AN INVALID SEQUENCE")
        result_search = re.finditer(r'[^atcg]+', adn_str)
        for i in result_search:
            print("The invalid character", i.group(),
                  "is at position", i.start())
